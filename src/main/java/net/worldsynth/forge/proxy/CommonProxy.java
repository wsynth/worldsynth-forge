/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge.proxy;

import java.io.File;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.worldsynth.WorldSynth;
import net.worldsynth.WorldSynthDirectoryConfig;
import net.worldsynth.addon.AddonMinecraft;
import net.worldsynth.addon.IAddon;
import net.worldsynth.forge.WorldSynthForge;

public class CommonProxy {

	public void preInit(FMLPreInitializationEvent event) {
		WorldSynthForge.LOGGER.info("Initializing WorldSynth Engine");
		
		//Get the root directory of the minecraft instance
		File mcDir = Loader.instance().getConfigDir().getParentFile();
		
		//Get the worldsynth directory for worldsynth configs and other 
		File worldSynthDir = new File(mcDir, "worldsynth");
		if(mcDir.exists() && !worldSynthDir.exists()) {
			worldSynthDir.mkdir();
		}
		
		//Get the worldsynth directory for worldsynth configs and other 
		File synthDir = new File(worldSynthDir, "synths");
		if(worldSynthDir.exists() && !synthDir.exists()) {
			synthDir.mkdir();
		}
		
		//Initialize the worldsynth engine
		// TODO Insert materials and bioles dir path
		File materialsDirectory = new File( "C:/Users/.../worldsynth-materials-minecraft/materials");
		File biomesDirectory = new File("C:/Users/.../worldsynth-materials-minecraft/biomes");
		WorldSynthDirectoryConfig directoryConfig = new WorldSynthDirectoryConfig(worldSynthDir, null, materialsDirectory, biomesDirectory);
		
		IAddon[] injectedAddons = { new AddonMinecraft() };
		
		new WorldSynth(directoryConfig, injectedAddons);
		
		WorldSynthForge.LOGGER.info("Done initializing WorldSynth Engine");
	}
}

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge.synth;

import java.util.Hashtable;

import net.worldsynth.addon.minecraft.module.anvil.ModuleMinecraftWorldExport;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.synth.Synth;

public class SynthWrapper {
	
	private final Synth synth;
	
	private final Hashtable<String, AbstractModule> synthParameters = new Hashtable<String, AbstractModule>();
	private final AbstractModule forgeExporterModule;

	private final boolean generateCaves;
	private final boolean generatePonds;
	private final boolean generateTrees;
	private final boolean generateGrass;
	
	public SynthWrapper(Synth synth) throws InvalidSynthException {
		this.synth = synth;
		
		forgeExporterModule = getForgeExportModule(synth);
		
		generateCaves = false;
		generatePonds = false;
		generateTrees = false;
		generateGrass = false;
		
		
		//TODO search for all parameter modules and add them to parameters table
	}
	
	public Synth getSynth() {
		return synth;
	}
	
	public String getName() {
		return synth.getName();
	}
	
	public DatatypeBlockspace getChunkBlockspace(int x, int z) {
		DatatypeBlockspace blockspace = new DatatypeBlockspace(new BuildExtent(x*16, 0, z*16, 16, 256, 16), 1.0);
		blockspace = (DatatypeBlockspace) forgeExporterModule.buildInputData(new ModuleInputRequest(forgeExporterModule.getInput(0), blockspace));
		return blockspace;
	}
	
	private ModuleMinecraftWorldExport getForgeExportModule(Synth synth) throws InvalidSynthException {
		for(ModuleWrapper moduleWrapper: synth.getPatch().getModuleWrapperList()) {
			if(moduleWrapper.module instanceof ModuleMinecraftWorldExport) {
				return (ModuleMinecraftWorldExport) moduleWrapper.module;
			}
		}
		throw new InvalidSynthException("Invalid synth, missing MC export module", synth);
	}
}

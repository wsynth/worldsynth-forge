/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge.world.biome;

import net.minecraft.world.biome.BiomeProvider;
import net.worldsynth.forge.synth.SynthWrapper;

public class BiomeProviderWorldSynth extends BiomeProvider {
	
	private final SynthWrapper generatorSynth;
	
	public BiomeProviderWorldSynth(SynthWrapper synth) {
		generatorSynth = synth;
	}
}

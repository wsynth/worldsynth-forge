/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge.world;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiCreateWorld;
import net.minecraft.init.Biomes;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeProvider;
import net.minecraft.world.biome.BiomeProviderSingle;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.worldsynth.forge.client.gui.customization.GuiCustomizationWorldSynthScreen;
import net.worldsynth.forge.synth.SynthWrapper;
import net.worldsynth.forge.world.gen.ChunkGeneratorWorldSynth;

public class WorldTypeWorldSynth extends WorldType {
	
	private final SynthWrapper generatorSynth;
	
	public WorldTypeWorldSynth(SynthWrapper synth) {
		super(synth.getName().substring(0, Math.min(15, synth.getName().length())));
		generatorSynth = synth;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public String getTranslationKey() {
		return getName();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public String getInfoTranslationKey() {
		return "A worldSynth world type";
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasInfoNotice() {
		return true;
	}
	
	@Override
	public boolean isCustomizable() {
		return false;
	}
	
	@Override
	public IChunkGenerator getChunkGenerator(World world, String generatorOptions) {
		return new ChunkGeneratorWorldSynth(world, world.getSeed(), generatorSynth, generatorOptions);
	}
	
	@Override
	public BiomeProvider getBiomeProvider(World world) {
		return new BiomeProviderSingle(Biomes.PLAINS);
//		return new BiomeProviderWorldSynth(generatorSynth);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void onCustomizeButton(Minecraft mc, GuiCreateWorld guiCreateWorld) {
		mc.displayGuiScreen(new GuiCustomizationWorldSynthScreen(guiCreateWorld, guiCreateWorld.chunkProviderSettingsJson));
	}
}

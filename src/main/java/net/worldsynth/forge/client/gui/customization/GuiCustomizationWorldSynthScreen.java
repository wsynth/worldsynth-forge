/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge.client.gui.customization;

import net.minecraft.client.gui.GuiCreateWorld;
import net.minecraft.client.gui.GuiPageButtonList;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.world.gen.ChunkGeneratorSettings;

public class GuiCustomizationWorldSynthScreen extends GuiScreen {
	private final GuiCreateWorld parent;
	
	private ChunkGeneratorSettings.Factory settings;
	
	public GuiCustomizationWorldSynthScreen(GuiScreen parentIn, String generatorSynthSettingsJson) {
		this.parent = (GuiCreateWorld)parentIn;
//        this.loadValues(generatorSynthSettingsJson);
	}
	
	@Override
	public void initGui() {
		super.initGui();
	}

}